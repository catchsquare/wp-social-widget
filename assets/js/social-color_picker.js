function social_color_picker(){
	
        // Add Color Picker to all inputs that have 'color-field' class
        jQuery( '.color-field' ).wpColorPicker();
}

jQuery(document).ready(function($){

    $(document).on('click', ' .scroll a', function(event){
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 1000);
    });

    $('.outline').click(function(){
        var getClassName = $(this).attr('class').split(" ")[1];
        $(this).toggleClass('active');
        $('.url-link.' + getClassName ).toggleClass('active');
    });
});